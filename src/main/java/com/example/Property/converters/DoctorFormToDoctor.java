package com.example.Property.converters;

import com.example.Property.commands.DoctorForm;
import com.example.Property.domain.Doctors;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class DoctorFormToDoctor implements Converter<DoctorForm, Doctors> {
    @Override
    public Doctors convert(DoctorForm doctorForm) {
        Doctors doctors = new Doctors();
        if (doctorForm.getIddoctor() != null  && !StringUtils.isEmpty(doctorForm.getIddoctor())) {
            doctors.setIddoctor(doctorForm.getIddoctor());
        }
        doctors.setFirstname(doctorForm.getFirstname());
        doctors.setLastname(doctorForm.getLastname());
        doctors.setAge( doctorForm.getAge());
        doctors.setSpecial( doctorForm.getSpecial());
        doctors.setSchedule(doctorForm.getSchedule());
        doctors.setPhonenum(doctorForm.getPhonenum());
        return doctors;
    }
}
