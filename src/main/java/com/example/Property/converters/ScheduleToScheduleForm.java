package com.example.Property.converters;

import com.example.Property.commands.SchedulesForm;
import com.example.Property.domain.Schedules;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ScheduleToScheduleForm implements Converter<Schedules, SchedulesForm> {
    @Override
    public SchedulesForm convert(Schedules schedules){
        SchedulesForm schedulesForm = new SchedulesForm();
        schedulesForm.setId(schedules.getId());
        schedulesForm.setWorkingdays(schedules.getWorkingdays());
    return schedulesForm;
    }
}
