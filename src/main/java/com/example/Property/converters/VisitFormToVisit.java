package com.example.Property.converters;


import com.example.Property.commands.VisitForm;
import com.example.Property.domain.Visits;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class VisitFormToVisit implements Converter<VisitForm, Visits> {
    @Override
    public Visits convert(VisitForm visitForm)
    {
        Visits visits = new Visits();
        if (visitForm.getIdvisit() != null  && !StringUtils.isEmpty(visitForm.getIdvisit())) {
            visits.setIdvisit(visitForm.getIdvisit());
        }
        visits.setIddoc(visitForm.getIddoc());
        visits.setVisitdate(visitForm.getVisitdate());
        visits.setTotalprice((visitForm.getTotalprice()));
        visits.setServicetype(visitForm.getServicetype());
        visits.setPacientname(visitForm.getPacientname());
        return visits;
    }
}
