package com.example.Property.converters;

import com.example.Property.commands.VisitForm;
import com.example.Property.domain.Visits;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class VisitToVisitForm implements Converter<Visits, VisitForm> {
    @Override
    public VisitForm convert(Visits visits) {
        VisitForm visitForm = new VisitForm();
        visitForm.setIdvisit(visits.getIdvisit());
        visitForm.setIddoc(visits.getIddoc());
        visitForm.setVisitdate(visits.getVisitdate());
        visitForm.setTotalprice(visits.getTotalprice());
        visitForm.setServicetype(visits.getServicetype());
        visitForm.setPacientname(visits.getPacientname());

        return visitForm;
    }
}


