package com.example.Property.converters;

import com.example.Property.commands.PatientForm;
import com.example.Property.domain.Patients;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class PatientFormToPatient implements Converter<PatientForm, Patients>  {
    @Override
    public Patients convert(PatientForm patientForm){
        Patients patients = new Patients();
        if (patientForm.getId() != null  && !StringUtils.isEmpty(patientForm.getId())) {
            patients.setId(patientForm.getId());
        }
        patients.setFirstname(patientForm.getFirstname());
        patients.setLastname(patientForm.getLastname());
        patients.setAge(patientForm.getAge());
        patients.setSerialpass(patientForm.getSerialpass());
        patients.setNumberpass(patientForm.getNumberpass());
        patients.setServicenam(patientForm.getServicenam());
        return patients;
    }
}