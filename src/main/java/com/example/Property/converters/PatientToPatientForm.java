package com.example.Property.converters;



import com.example.Property.commands.PatientForm;

import com.example.Property.domain.Patients;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PatientToPatientForm implements Converter<Patients, PatientForm> {
    @Override
    public PatientForm convert(Patients patients) {
        PatientForm patientForm = new PatientForm();
        patientForm.setId(patients.getId());
        patientForm.setFirstname(patients.getFirstname());
        patientForm.setLastname(patients.getLastname());
        patientForm.setAge(patients.getAge());
        patientForm.setSerialpass(patients.getSerialpass());
        patientForm.setNumberpass(patients.getNumberpass());
        patientForm.setServicenam(patients.getServicenam());
        return patientForm;
    }
}

