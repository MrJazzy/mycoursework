package com.example.Property.converters;

import com.example.Property.commands.UslugaForm;
import com.example.Property.domain.Uslugas;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class UslugaFormToUsluga implements Converter<UslugaForm, Uslugas> {
    @Override
    public Uslugas convert(UslugaForm uslugaForm)
    {
        Uslugas uslugas = new Uslugas();
        if (uslugaForm.getIdservice() != null  && !StringUtils.isEmpty(uslugaForm.getIdservice())) {
            uslugas.setIdservice(uslugaForm.getIdservice());
        }
        uslugas.setPrice(uslugaForm.getPrice());
        uslugas.setServicename(uslugaForm.getServicename());
        return uslugas;
    }
}
