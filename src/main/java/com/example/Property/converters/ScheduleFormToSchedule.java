package com.example.Property.converters;

import com.example.Property.commands.SchedulesForm;
import com.example.Property.domain.Schedules;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ScheduleFormToSchedule implements Converter<SchedulesForm, Schedules> {
    @Override
    public Schedules convert(SchedulesForm schedulesForm)
    {
        Schedules schedules = new Schedules();
        if (schedulesForm.getId() != null  && !StringUtils.isEmpty(schedulesForm.getId())) {
            schedules.setId(schedulesForm.getId());
        }
        schedules.setWorkingdays(schedulesForm.getWorkingdays());
        return schedules;
    }
}
