package com.example.Property.converters;


import com.example.Property.commands.UslugaForm;
import com.example.Property.domain.Uslugas;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UslugaToUslugaForm implements Converter<Uslugas, UslugaForm> {
    @Override
    public UslugaForm convert(Uslugas uslugas) {
        UslugaForm uslugaForm = new UslugaForm();
        uslugaForm.setIdservice(uslugas.getIdservice());
        uslugaForm.setPrice(uslugas.getPrice());
        uslugaForm.setServicename(uslugas.getServicename());
        return uslugaForm;
    }
}

