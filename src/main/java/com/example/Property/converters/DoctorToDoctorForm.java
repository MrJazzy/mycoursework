package com.example.Property.converters;

import com.example.Property.commands.DoctorForm;
import com.example.Property.domain.Doctors;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DoctorToDoctorForm implements Converter<Doctors, DoctorForm>{

    @Override
    public DoctorForm convert(Doctors doctors) {
        DoctorForm doctorForm = new DoctorForm();
        doctorForm.setIddoctor(doctors.getIddoctor());
        doctorForm.setFirstname(doctors.getFirstname());
        doctorForm.setLastname(doctors.getLastname());
        doctorForm.setAge( doctors.getAge());
        doctorForm.setSpecial( doctors.getSpecial());
        doctorForm.setSchedule(doctors.getSchedule());
        doctorForm.setPhonenum(doctors.getPhonenum());
        return doctorForm;
    }
}
