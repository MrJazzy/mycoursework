package com.example.Property.repositories;

import com.example.Property.domain.Patients;
import org.springframework.data.repository.CrudRepository;

public interface PatientRepo extends CrudRepository<Patients, Long> {
}
