package com.example.Property.repositories;

import com.example.Property.domain.Doctors;
import org.springframework.data.repository.CrudRepository;

public interface DoctorRepo extends CrudRepository<Doctors, Long> {


}
