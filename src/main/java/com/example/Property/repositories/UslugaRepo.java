package com.example.Property.repositories;

import com.example.Property.domain.Uslugas;
import org.springframework.data.repository.CrudRepository;

public interface UslugaRepo extends CrudRepository<Uslugas, Long> {
}
