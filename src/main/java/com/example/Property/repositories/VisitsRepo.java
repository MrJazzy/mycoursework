package com.example.Property.repositories;

import com.example.Property.domain.Visits;
import org.springframework.data.repository.CrudRepository;

public interface VisitsRepo extends CrudRepository<Visits, Long> {
}
