package com.example.Property.repositories;

import com.example.Property.domain.Schedules;
import org.springframework.data.repository.CrudRepository;

public interface ScheduleRepo extends CrudRepository<Schedules, Long> {
}
