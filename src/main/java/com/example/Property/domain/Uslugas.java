package com.example.Property.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Uslugas {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idservice;
    private Integer price;
    private String servicename;

    public Long getIdservice() {
        return idservice;
    }

    public void setIdservice(Long idservice) {
        this.idservice = idservice;
    }

    public String getServicename() {
        return servicename;
    }

    public void setServicename(String servicename) {
        this.servicename = servicename;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
