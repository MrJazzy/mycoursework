package com.example.Property.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Patients {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstname;
    private String lastname;
    private Integer age;
    private Integer serialpass;
    private Integer numberpass;
    private Integer servicenam;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSerialpass() {
        return serialpass;
    }

    public void setSerialpass(Integer serialpass) {
        this.serialpass = serialpass;
    }

    public Integer getNumberpass() {
        return numberpass;
    }

    public void setNumberpass(Integer numberpass) {
        this.numberpass = numberpass;
    }

    public Integer getServicenam() {
        return servicenam;
    }

    public void setServicenam(Integer servicenam) {
        this.servicenam = servicenam;
    }
}
