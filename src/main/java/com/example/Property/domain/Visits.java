package com.example.Property.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Visits {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idvisit;

    private Integer iddoc;

    private String visitdate;
    private Integer totalprice;
    private String servicetype;
    private String pacientname;

    public Long getIdvisit() {
        return idvisit;
    }

    public void setIdvisit(Long idvisit) {
        this.idvisit = idvisit;
    }

    public Integer getIddoc() {
        return iddoc;
    }

    public void setIddoc(Integer iddoc) {
        this.iddoc = iddoc;
    }

    public String getVisitdate() {
        return visitdate;
    }

    public void setVisitdate(String visitdate) {
        this.visitdate = visitdate;
    }

    public Integer getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Integer totalprice) {
        this.totalprice = totalprice;
    }

    public String getServicetype() {
        return servicetype;
    }

    public void setServicetype(String servicetype) {
        this.servicetype = servicetype;
    }

    public String getPacientname() {
        return pacientname;
    }
    public void setPacientname(String pacientname) {
        this.pacientname = pacientname;
    }



}
