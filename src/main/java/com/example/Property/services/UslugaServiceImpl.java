package com.example.Property.services;

import com.example.Property.commands.UslugaForm;
import com.example.Property.converters.UslugaFormToUsluga;
import com.example.Property.domain.Uslugas;
import com.example.Property.repositories.UslugaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UslugaServiceImpl implements UslugaService {
    private UslugaRepo uslugaRepo;
    private UslugaFormToUsluga uslugaFormToUsluga;

    @Autowired
    public UslugaServiceImpl(UslugaRepo uslugaRepo, UslugaFormToUsluga uslugaFormToUsluga){
        this.uslugaRepo = uslugaRepo;
        this.uslugaFormToUsluga = uslugaFormToUsluga;
    }

    @Override
    public List<Uslugas> listAll() {
        List<Uslugas> uslugas = new ArrayList<>();
        uslugaRepo.findAll().forEach(uslugas::add);
        return uslugas;
    }

    @Override
    public Uslugas getById(Long idservice) {
        return uslugaRepo.findById(idservice).orElse(null);
    }

    @Override
    public Uslugas saveOrUpdate(Uslugas uslugas) {
        uslugaRepo.save(uslugas);
        System.out.println("Save Service Id: " + uslugas);
        return uslugas;
    }

    @Override
    public void delete(Long idservice) {
        uslugaRepo.deleteById(idservice);
        System.out.println("Delete Service Id: " + idservice);
    }

    @Override
    public Uslugas saveOrUpdateServiceForm(UslugaForm uslugaForm) {
        Uslugas savedUsluga = saveOrUpdate(uslugaFormToUsluga.convert(uslugaForm));

        System.out.println("Saved Service Id: " + savedUsluga.getIdservice());
        return savedUsluga;
    }
}

