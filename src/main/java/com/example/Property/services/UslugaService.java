package com.example.Property.services;

import com.example.Property.commands.UslugaForm;
import com.example.Property.domain.Uslugas;

import java.util.List;

public interface UslugaService {
    List<Uslugas> listAll();
    Uslugas getById(Long idservice);
    Uslugas saveOrUpdate(Uslugas uslugas);
    void delete(Long idservice);
    Uslugas saveOrUpdateServiceForm(UslugaForm uslugaForm);
}
