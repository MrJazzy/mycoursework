package com.example.Property.services;

import com.example.Property.commands.VisitForm;
import com.example.Property.converters.VisitFormToVisit;
import com.example.Property.domain.Visits;
import com.example.Property.repositories.VisitsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VisitServiceImpl implements VisitService {
    private VisitsRepo visitsRepo;
    private VisitFormToVisit visitFormToVisit;

    @Autowired
    public VisitServiceImpl(VisitsRepo visitsRepo, VisitFormToVisit visitFormToVisit){
        this.visitsRepo = visitsRepo;
        this.visitFormToVisit = visitFormToVisit;
    }

    @Override
    public List<Visits> listAll() {
        List<Visits> visits = new ArrayList<>();
        visitsRepo.findAll().forEach(visits::add);
        return visits;
    }

    @Override
    public Visits getById(Long idvisit) {
        return visitsRepo.findById(idvisit).orElse(null);
    }

    @Override
    public Visits saveOrUpdate(Visits visits) {
        visitsRepo.save(visits);
        System.out.println("Save Visit Id: " + visits);
        return visits;
    }

    @Override
    public void delete(Long idvisit) {
        visitsRepo.deleteById(idvisit);
        System.out.println("Delete Visit Id: " + idvisit);
    }

    @Override
    public Visits saveOrUpdateVisitForm(VisitForm visitForm) {
        Visits savedVisit = saveOrUpdate(visitFormToVisit.convert(visitForm));

        System.out.println("Saved Visit Id: " + savedVisit.getIdvisit());
        return savedVisit;
    }
}