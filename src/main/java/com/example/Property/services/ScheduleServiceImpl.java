package com.example.Property.services;

import com.example.Property.commands.SchedulesForm;
import com.example.Property.converters.ScheduleFormToSchedule;
import com.example.Property.domain.Schedules;
import com.example.Property.repositories.ScheduleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    private ScheduleRepo scheduleRepo;
    private ScheduleFormToSchedule scheduleFormToSchedule;

    @Autowired
    public ScheduleServiceImpl(ScheduleRepo scheduleRepo, ScheduleFormToSchedule scheduleFormToSchedule){
        this.scheduleRepo = scheduleRepo;
        this.scheduleFormToSchedule = scheduleFormToSchedule;
    }

    @Override
    public List<Schedules> listAll() {
        List<Schedules> schedules = new ArrayList<>();
        scheduleRepo.findAll().forEach(schedules::add);
        return schedules;
    }

    @Override
    public Schedules getById(Long id) {
        return scheduleRepo.findById(id).orElse(null);
    }

    @Override
    public Schedules saveOrUpdate(Schedules schedules) {
        scheduleRepo.save(schedules);
        System.out.println("Save Schedule Id: " + schedules);
        return schedules;
    }

    @Override
    public void delete(Long id) {
        scheduleRepo.deleteById(id);
        System.out.println("Delete Schedule Id: " + id);
    }

    @Override
    public Schedules saveOrUpdateScheduleForm(SchedulesForm schedulesForm) {
        Schedules savedSchedules = saveOrUpdate(scheduleFormToSchedule.convert(schedulesForm));

        System.out.println("Saved Schedule Id: " + savedSchedules.getId());
        return savedSchedules;
    }


}
