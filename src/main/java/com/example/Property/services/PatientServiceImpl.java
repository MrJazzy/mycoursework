package com.example.Property.services;

import com.example.Property.commands.PatientForm;
import com.example.Property.converters.PatientFormToPatient;
import com.example.Property.domain.Patients;
import com.example.Property.repositories.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {
    private PatientRepo patientRepo;
    private PatientFormToPatient patientFormToPatient;

    @Autowired
    public PatientServiceImpl(PatientRepo patientRepo, PatientFormToPatient patientFormToPatient){
        this.patientRepo = patientRepo;
        this.patientFormToPatient = patientFormToPatient ;
    }

    @Override
    public List<Patients> listAll() {
        List<Patients> patients = new ArrayList<>();
        patientRepo.findAll().forEach(patients::add);
        return patients;
    }

    @Override
    public Patients getById(Long id) {
        return patientRepo.findById(id).orElse(null);
    }

    @Override
    public Patients saveOrUpdate(Patients patients) {
       patientRepo.save(patients);
        System.out.println("Save Patient Id: " + patients);
        return patients;
    }

    @Override
    public void delete(Long id) {
        patientRepo.deleteById(id);
        System.out.println("Delete Patient Id: " + id);
    }

    @Override
    public Patients saveOrUpdatePatientForm(PatientForm patientForm) {
        Patients savedPatient = saveOrUpdate(patientFormToPatient.convert(patientForm));

        System.out.println("Saved Patient Id: " + savedPatient.getId());
        return savedPatient;
    }
}

