package com.example.Property.services;

import com.example.Property.commands.DoctorForm;
import com.example.Property.converters.DoctorFormToDoctor;
import com.example.Property.domain.Doctors;
import com.example.Property.repositories.DoctorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DoctorServiceImpl implements DoctorService {

    private DoctorRepo doctorRepo;
    private DoctorFormToDoctor doctorFormToDoctor;



    @Autowired
    public DoctorServiceImpl(DoctorRepo doctorRepo, DoctorFormToDoctor doctorFormToDoctor){
        this.doctorRepo = doctorRepo;
        this.doctorFormToDoctor = doctorFormToDoctor;
    }

    @Override
    public List<Doctors> listAll() {
        List<Doctors> doctors = new ArrayList<>();
        doctorRepo.findAll().forEach(doctors::add);
        return doctors;
    }

    @Override
    public Doctors getById(Long iddoctor) {
        return doctorRepo.findById(iddoctor).orElse(null);
    }

    @Override
    public Doctors saveOrUpdate(Doctors doctors) {
        doctorRepo.save(doctors);
        System.out.println("Save Doctor Id: " + doctors);

        return doctors;
    }

    @Override
    public void delete(Long iddoctor) {
        doctorRepo.deleteById(iddoctor);
        System.out.println("Delete Doctor Id: " + iddoctor);
    }

    @Override
    public Doctors saveOrUpdateDoctorForm(DoctorForm doctorForm) {
        Doctors savedDoctor = saveOrUpdate(doctorFormToDoctor.convert(doctorForm));

        System.out.println("Saved Doctor Id: " + savedDoctor.getIddoctor());
        return savedDoctor;
    }
}
