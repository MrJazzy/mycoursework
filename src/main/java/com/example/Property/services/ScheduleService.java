package com.example.Property.services;

import com.example.Property.commands.SchedulesForm;
import com.example.Property.domain.Schedules;

import java.util.List;

public interface ScheduleService {
    List<Schedules> listAll();
    Schedules getById(Long id);
    Schedules saveOrUpdate(Schedules schedules);
    void delete(Long id);
    Schedules saveOrUpdateScheduleForm(SchedulesForm schedulesForm);
}
