package com.example.Property.services;

import com.example.Property.commands.DoctorForm;
import com.example.Property.domain.Doctors;

import java.util.Collection;
import java.util.List;

public interface DoctorService {
    List<Doctors> listAll();
    Doctors getById(Long iddoctor);
    Doctors saveOrUpdate(Doctors doctors);
    void delete(Long iddoctor);
    Doctors saveOrUpdateDoctorForm(DoctorForm doctorForm);

}
