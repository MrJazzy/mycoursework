package com.example.Property.services;

import com.example.Property.commands.VisitForm;
import com.example.Property.domain.Visits;

import java.util.List;

public interface VisitService {
    List<Visits> listAll();
    Visits getById(Long idvisit);
    Visits saveOrUpdate(Visits visits);
    void delete(Long idvisit);
    Visits saveOrUpdateVisitForm(VisitForm visitForm);
}
