package com.example.Property.services;

import com.example.Property.commands.PatientForm;
import com.example.Property.domain.Patients;

import java.util.List;

public interface PatientService {
    List<Patients> listAll();
    Patients getById(Long id);
    Patients saveOrUpdate(Patients patients);
    void delete(Long id);
    Patients saveOrUpdatePatientForm(PatientForm patientForm);


}
