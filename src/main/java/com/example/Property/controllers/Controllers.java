package com.example.Property.controllers;

import com.example.Property.commands.*;
import com.example.Property.converters.*;
import com.example.Property.domain.*;
import com.example.Property.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@org.springframework.stereotype.Controller
public class Controllers {
    private DoctorService doctorService;
    private DoctorToDoctorForm doctorToDoctorForm;
    private ScheduleService scheduleService;
    private ScheduleToScheduleForm scheduleToScheduleForm;
    private PatientToPatientForm patientToPatientForm;
    private PatientService patientService;
    private UslugaService uslugaService;
    private UslugaToUslugaForm uslugaToUslugaForm;
    private VisitService visitService;
    private VisitToVisitForm visitToVisitForm;

    @Autowired
    public void setScheduleToScheduleForm(ScheduleToScheduleForm scheduleToScheduleForm){
        this.scheduleToScheduleForm = scheduleToScheduleForm;
    }
    @Autowired
    public void setScheduleService(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }
    @Autowired
    public void setDoctorToDoctorForm(DoctorToDoctorForm doctorToDoctorForm) {
        this.doctorToDoctorForm = doctorToDoctorForm;
    }

    @Autowired
    public void setDoctorService(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @Autowired
    public void setPatientToPatientForm(PatientToPatientForm patientToPatientForm){
        this.patientToPatientForm = patientToPatientForm;
    }
    @Autowired
    public void setUslugaService(UslugaService uslugaService){this.uslugaService = uslugaService;
    }

    @Autowired
    public void setServiceToServiceFrom(UslugaToUslugaForm serviceToServiceFrom){
        this.uslugaToUslugaForm = serviceToServiceFrom;
    }
    @Autowired
    public void setPatientService(PatientService patientService){this.patientService = patientService;
    }

    @Autowired
    public void setVisitToVisitFrom(VisitToVisitForm visitToVisitFrom){
        this.visitToVisitForm = visitToVisitFrom;
    }
    @Autowired
    public void setVisitService(VisitService visitService){this.visitService = visitService;
    }

    @RequestMapping("/")
    public String redirToList(){
        return "redirect:/doctor/list";
    }

    @RequestMapping({"/doctor/list", "/doctor"})
    public String listDoctor(Model model){
        model.addAttribute("doctors", doctorService.listAll());
        model.addAttribute("schedules",scheduleService.listAll());
        return "doctor/list";
    }

    @RequestMapping("/doctor/show/{iddoctor}")
    public String getDoctor(@PathVariable String iddoctor, Model model){
        model.addAttribute("doctors", doctorService.getById(Long.valueOf(iddoctor)));
        return "doctor/show";
    }

    @RequestMapping("doctor/edit/{iddoctor}")
    public String editDoctor(@PathVariable String iddoctor, Model model){
        Doctors doctors = doctorService.getById(Long.valueOf(iddoctor));
        DoctorForm doctorForm = doctorToDoctorForm.convert(doctors);

        model.addAttribute("doctorForm", doctorForm);
        return "doctor/doctorform";
    }

    @RequestMapping("/doctor/new")
    public String newDoctor(Model model){
        model.addAttribute("doctorForm", new DoctorForm());
        return "doctor/doctorform";
    }

    @RequestMapping(value = "/doctor", method = RequestMethod.POST)
    public String saveOrUpdateDoctor(@Valid DoctorForm doctorForm, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "doctor/doctorform";
        }

        Doctors savedDoctor = doctorService.saveOrUpdateDoctorForm(doctorForm);

        return "redirect:/doctor/show/" + savedDoctor.getIddoctor();
    }

    @RequestMapping("/doctor/delete/{iddoctor}")
    public String delete(@PathVariable String iddoctor){
        doctorService.delete(Long.valueOf(iddoctor));
        return "redirect:/doctor/list";
    }

    @RequestMapping("/schedule/new")
    public String newSchedule(Model model){
        model.addAttribute("schedulesForm", new SchedulesForm());
        return "schedule/scheduleform";
    }

    @RequestMapping({"/schedule/list", "/schedule"})
    public String listSchedule(Model model){
        model.addAttribute("schedules", scheduleService.listAll());
        return "schedule/list";
    }

    @RequestMapping("/schedule/show/{id}")
    public String getSchedule(@PathVariable String id, Model model){
        model.addAttribute("schedules", scheduleService.getById(Long.valueOf(id)));
        return "schedule/show";
    }

    @RequestMapping("schedule/edit/{id}")
    public String editSchedule(@PathVariable String id, Model model){
        Schedules schedules = scheduleService.getById(Long.valueOf(id));
        SchedulesForm schedulesForm = scheduleToScheduleForm.convert(schedules);

        model.addAttribute("schedulesForm", schedulesForm);
        return "schedule/scheduleform";
    }

    @RequestMapping(value = "/schedule", method = RequestMethod.POST)
    public String saveOrUpdateSchedule(@Valid SchedulesForm schedulesForm, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "schedule/scheduleform";
        }

        Schedules savedSchedules = scheduleService.saveOrUpdateScheduleForm(schedulesForm);

        return "redirect:/schedule/show/" + savedSchedules.getId();
    }
    @RequestMapping("/schedule/delete/{id}")
    public String deleteSched(@PathVariable String id){
        scheduleService.delete(Long.valueOf(id));
        return "redirect:/schedule/list";
    }
    @RequestMapping("/patient/new")
    public String newPatient(Model model){
        model.addAttribute("patientForm", new PatientForm());
        model.addAttribute("uslugas", uslugaService.listAll());
        return "patient/patientform";
    }

    @RequestMapping({"/patient/list", "/patient"})
    public String listPatient(Model model){
        model.addAttribute("patients", patientService.listAll());
        model.addAttribute("uslugas", uslugaService.listAll());
        return "patient/list";
    }

    @RequestMapping("/patient/show/{id}")
    public String getPatient(@PathVariable String id, Model model){
        model.addAttribute("patients", patientService.getById(Long.valueOf(id)));
        return "patient/show";
    }

    @RequestMapping("patient/edit/{id}")
    public String editPatient(@PathVariable String id, Model model){
        Patients patients = patientService.getById(Long.valueOf(id));
        PatientForm patientForm = patientToPatientForm.convert(patients);

        model.addAttribute("patientForm", patientForm);
        return "patient/patientForm";
    }

    @RequestMapping(value = "/patient", method = RequestMethod.POST)
    public String saveOrUpdatePatient(@Valid PatientForm patientForm, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "patient/patientform";
        }

        Patients savedPatient = patientService.saveOrUpdatePatientForm(patientForm);

        return "redirect:/patient/show/" + savedPatient.getId();
    }
    @RequestMapping("/patient/delete/{id}")
    public String deletePatient(@PathVariable String id){
        patientService.delete(Long.valueOf(id));
        return "redirect:/patient/list";
    }

    @RequestMapping("/service/new")
    public String newService(Model model){
        model.addAttribute("uslugaForm", new UslugaForm());
        return "service/serviceform";
    }

    @RequestMapping({"/service/list", "/service"})
    public String listService(Model model){
        model.addAttribute("uslugas", uslugaService.listAll());
        return "service/list";
    }

    @RequestMapping("/service/show/{idservice}")
    public String getService(@PathVariable String idservice, Model model){
        model.addAttribute("uslugas", uslugaService.getById(Long.valueOf(idservice)));
        return "service/show";
    }

    @RequestMapping("service/edit/{idservice}")
    public String editService(@PathVariable String idservice, Model model){
        Uslugas uslugas = uslugaService.getById(Long.valueOf(idservice));
        UslugaForm uslugaForm = uslugaToUslugaForm.convert(uslugas);

        model.addAttribute("uslugaForm", uslugaForm);
        return "service/serviceform";
    }

    @RequestMapping(value = "/service", method = RequestMethod.POST)
    public String saveOrUpdateService(@Valid UslugaForm uslugaForm, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "service/serviceform";
        }

        Uslugas savedService = uslugaService.saveOrUpdateServiceForm(uslugaForm);

        return "redirect:/service/show/" + savedService.getIdservice();
    }
    @RequestMapping("/service/delete/{idservice}")
    public String deleteService(@PathVariable String idservice){
        uslugaService.delete(Long.valueOf(idservice));
        return "redirect:/service/list";
    }

    @RequestMapping("/visit/new")
    public String newVisit(Model model){
        model.addAttribute("visitForm", new VisitForm());
        model.addAttribute("doctors", doctorService.listAll());
        model.addAttribute("uslugas", uslugaService.listAll());
        model.addAttribute("patients",patientService.listAll());
        return "visit/visitform";
    }

    @RequestMapping({"/visit/list", "/visit"})
    public String listVisit(Model model){
        model.addAttribute("visits", visitService.listAll());
        return "visit/list";
    }

    @RequestMapping("/visit/show/{idvisit}")
    public String getVisit(@PathVariable String idvisit, Model model){
        model.addAttribute("visits", visitService.getById(Long.valueOf(idvisit)));
        return "visit/show";
    }

    @RequestMapping("visit/edit/{idvisit}")
    public String editVisit(@PathVariable String idvisit, Model model){
        Visits visits = visitService.getById(Long.valueOf(idvisit));
        VisitForm visitForm = visitToVisitForm.convert(visits);

        model.addAttribute("visitForm", visitForm);
        model.addAttribute("uslugas",uslugaService.listAll());
        model.addAttribute("doctors",doctorService.listAll());
        model.addAttribute("patients",patientService.listAll());
        return "visit/visitform";
    }

    @RequestMapping(value = "/visit", method = RequestMethod.POST)
    public String saveOrUpdateVisit(@Valid VisitForm visitForm, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "visit/visitform";
        }

        Visits savedVisit = visitService.saveOrUpdateVisitForm(visitForm);

        return "redirect:/visit/show/" + savedVisit.getIdvisit();
    }
    @RequestMapping("/visit/delete/{idvisit}")
    public String deleteVisit(@PathVariable String idvisit){
        visitService.delete(Long.valueOf(idvisit));
        return "redirect:/visit/list";
    }

}
